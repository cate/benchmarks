===============
cate-benchmarks
===============

CATE stands for **C**\ haotic **A**\ ttractor **TE**\ mplate.

``cate`` is a libre software tool (licensed under GNU Lesser General Public
License v3.0 only) to draw the templates of chaotic attractors.

----

This repository contains the necessary materials to benchmark ``cate``.  If you
are interested in the tool itself, please refer to `cate repository`_.

The repository is organized as follow:
  .. code:: sh

     .
     ├── inputs/     # input linking matrices as minified JSON files
     └── README.rst  # this very file

.. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. .. ..

.. _`cate repository`: https://gitlab.inria.fr/cate/cate
